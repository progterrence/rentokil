# -*- coding: utf-8 -*-
import base64
from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import itertools
from datetime import datetime
from odoo.osv import osv
import logging
from odoo.exceptions import UserError
_logger = logging.getLogger(__name__)
import tempfile
import binascii
from odoo.tools.safe_eval import safe_eval

try:
    import xlrd
    try:
        from xlrd import xlsx
    except ImportError:
        xlsx = None
except ImportError:
    xlrd = xlsx = None


class ImportResPartnerLog(models.TransientModel):
    _name = 'import.res.partner.log'
    _description = 'Import Products Logs'

    wiz_id = fields.Many2one('import.res.partner')
    name = fields.Char()
    row_no = fields.Integer()
    account_no = fields.Char()
    email = fields.Char()
    error = fields.Char()


class LineImport(models.TransientModel):
    _name = 'import.res.partner'
    _description = 'Import Contacts'

    data_file = fields.Binary(string='Contact File', required=True)
    filename = fields.Char()
    import_successful = fields.Boolean(default=False)
    error_logs = fields.One2many('import.res.partner.log', 'wiz_id')

    @api.multi
    def import_file(self):
        active_id = self._context.get('active_id', False)
        active_model = self._context.get('active_model', False)
        self.ensure_one()
        """Load Inventory data from the CSV file."""
        if not self.data_file:
            raise UserError('Excel File is missing')
        fp = tempfile.NamedTemporaryFile(delete=False, suffix=".xlsx")
        fp.write(binascii.a2b_base64(self.data_file))
        fp.seek(0)
        values = {}
        workbook = xlrd.open_workbook(fp.name)
        sheet = workbook.sheet_by_index(0)
        Partner = self.env['res.partner']
        ErrorObj = self.env['import.res.partner.log']
        active_id = self._context.get('active_id', False)
        active_model = self._context.get('active_model', False)
        lines = []
        for row_no in range(sheet.nrows):
            val = {}
            # print(row_no)
            if row_no <= 0:
                fields = list(map(lambda row: str(row.value).encode('utf-8'),sheet.row(row_no)))
            else:
                row = list(map(lambda row: isinstance(row.value,bytes) and str(row.value).encode('utf-8') or str(row.value), sheet.row(row_no)))
                if row:
                    if not any(row):
                        continue
                    try:
                        log_row_no = row_no + 1
                        error = False
                        if not row[0] or row[0] == "":
                            error = "Missing Account Number on row %s." % log_row_no
                        if not row[1] or row[1] == "":
                            error = "Missing Email on row %s." % log_row_no
                        if not row[2] or row[2] == "":
                            error = "Missin Name on row %s." % log_row_no
                        if error:
                            e_vals = {
                                'error': error,
                                'wiz_id': self.id,
                                'account_number': row[0],
                                'email': row[1],
                                'name': row[2],
                            }
                            ErrorObj.create(e_vals)
                        else:
                            row_code = row[0]
                            if row[0].endswith('.0'):
                                row_code = row[0][:-2]
                            values.update({
                                'account_number': row_code,
                                'email': row[1],
                                'name': row[2]
                            })
                            existing_account = Partner.search([
                                ('account_number', '=', values['account_number'])], limit=1)
                            if existing_account:
                                existing_account.write({
                                    'email': values['email'],
                                    'name': values['name']
                                })
                            else:
                                Partner.create(values)
                            self.import_successful = True
                    except Exception as e:
                        #TODO: Show user error
                        print('error2 %s' % e)
                        raise UserError("Import Error: %s" % e)
