# -*- coding: utf-8 -*-
{
    'name': 'Import Contacts Rentokil',
    'version': '1.0',
    'summary': 'Import Contacts Rentokil',
    'author': 'Rentokil',
    'images': [],
    'depends': [
        'base'
    ],
    'data': [
        'wizard/import_res_partner.xml',
        'views/res_partner.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
