# Copyright 2021 Tecnativa - Carlos Roca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
from odoo import _, models


class ExportInvoiceErrorXlsx(models.AbstractModel):
    _name = 'report.rentokil.export_invoice_error_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    _description = 'Abstract model to export as xlsx the invoice'

    def _get_lang(self, user_id):
        lang_code = self.env['res.users'].browse(user_id).lang
        return self.env['res.lang']._lang_get(lang_code)

    def _create_error_sheet(self, workbook, book):
        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'left',
            'valign': 'vjustify',
        })
        header_format = workbook.add_format(
            {
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vjustify',
                'fg_color': '#F2F2F2'
            }
        )
        sheet = workbook.add_worksheet(_('Error Log'))
        # Header construction
        next_col = 0
        sheet.write(0, next_col, _('Customer'), header_format)
        next_col += 1
        sheet.write(0, next_col, _('Account Number'), header_format)
        next_col += 1
        sheet.write(0, next_col, _('Error'), header_format)
        next_col += 1
        sheet.write(0, next_col, _('File Path'), header_format)
        return sheet

    def _fill_data(self, workbook, sheet, book):
        row = 0
        for log in book.log_ids:
            row += 1
            next_col = 0
            if log.partner_id:
                sheet.write(row, next_col, log.partner_id.name)
            next_col += 1
            sheet.write(row, next_col, log.account_number)
            next_col += 1
            sheet.write(row, next_col, log.name)
            next_col += 1
            sheet.write(row, next_col, log.account_number)

        # bold_format = workbook.add_format({
        #     'bold': 1,
        # })
        # decimal_format = workbook.add_format({'num_format': '0.00'})
        # decimal_bold_format = workbook.add_format({
        #     'num_format': '0.00',
        #     'bold': 1,
        # })
        # row = 1
        # for group in book.get_groups_to_print():
        #     sheet.write(row, 0, group['group_name'], bold_format)
        #     row += 1
        #     for product in group['products']:
        #         next_col = 0
        #         sheet.write(row, next_col, product.display_name)
        #         if book.show_standard_price:
        #             next_col += 1
        #             sheet.write(row, next_col, product.standard_price, decimal_format)
        #         if book.show_sale_price:
        #             next_col += 1
        #             sheet.write(row, next_col, product.list_price, decimal_format)
        #         next_col += 1
        #         sheet.write(
        #             row,
        #             next_col,
        #             product.with_context(
        #                 pricelist=pricelist.id, date=book.date).price,
        #             decimal_bold_format
        #         )
        return sheet

    def generate_xlsx_report(self, workbook, data, objects):
        book = objects[0]
        sheet = self._create_error_sheet(workbook, book)
        sheet = self._fill_data(workbook, sheet, book)
