# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from . import res_config_settings
from . import invoice_batch
from . import res_partner
from . import mail_mail