
from odoo import models, api, fields


class MailValidationSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    validate_email = fields.Boolean('Validate Email')
    origin_folder = fields.Char('Origin Folder')
    process_file = fields.Boolean('Process single file')
    invoice_batch_folder = fields.Char('Invoice Batch Folder')
    origin_folder_unsigned = fields.Char('Invoice Folder (Unsigned)')
    origin_folder_in = fields.Char('MicroboxIn folder')
    logo_file = fields.Char('Logo File')
    destination_folder = fields.Char('Destination Folder')
    send_mail = fields.Boolean()
    merge_final_pdf = fields.Boolean()
    add_logo = fields.Boolean()
    clean_origin_folder = fields.Boolean()
    send_to_gdrive = fields.Boolean('Send to Gdrive')
    automatic_processing = fields.Boolean('Automatic Processing')
    gdrive_path = fields.Char('GDrive Path')
    feeder_file_path = fields.Char('Feeder File Path')

    @api.model
    def get_values(self):
        res = super(MailValidationSettings, self).get_values()
        config_parameters = self.env["ir.config_parameter"].sudo()
        res.update(
            validate_email=config_parameters.sudo().get_param('validate_email'),
            origin_folder=config_parameters.sudo().get_param('origin_folder'),
            invoice_batch_folder=config_parameters.sudo().get_param('invoice_batch_folder'),
            origin_folder_unsigned=config_parameters.sudo().get_param('origin_folder_unsigned'),
            origin_folder_in=config_parameters.sudo().get_param('origin_folder_in'),
            process_file=config_parameters.sudo().get_param('process_file'),
            logo_file=config_parameters.sudo().get_param('logo_file'),
            destination_folder=config_parameters.sudo().get_param('destination_folder'),
            send_mail=config_parameters.sudo().get_param('send_mail'),
            merge_final_pdf=config_parameters.sudo().get_param('merge_final_pdf'),
            add_logo=config_parameters.sudo().get_param('add_logo'),
            clean_origin_folder=config_parameters.sudo().get_param('clean_origin_folder'),
            automatic_processing=config_parameters.sudo().get_param('automatic_processing'),
            send_to_gdrive=config_parameters.sudo().get_param('send_to_gdrive'),
            gdrive_path=config_parameters.sudo().get_param('gdrive_path'),
            feeder_file_path=config_parameters.sudo().get_param('feeder_file_path')
        )
        return res

    @api.multi
    def set_values(self):
        super(MailValidationSettings, self).set_values()
        config_parameters = self.env["ir.config_parameter"].sudo()
        for record in self:
            config_parameters.set_param("validate_email", record.validate_email or '')
            config_parameters.set_param("origin_folder", record.origin_folder or '')
            config_parameters.set_param("invoice_batch_folder", record.invoice_batch_folder or '')
            config_parameters.set_param("origin_folder_unsigned",record.origin_folder_unsigned or '')
            config_parameters.set_param("origin_folder_in",record.origin_folder_in or '')
            config_parameters.set_param("process_file", record.process_file or '')
            config_parameters.set_param("logo_file", record.logo_file or '')
            config_parameters.set_param("destination_folder", record.destination_folder or '')
            config_parameters.set_param("send_mail", record.send_mail or '')
            config_parameters.set_param("merge_final_pdf", record.merge_final_pdf or '')
            config_parameters.set_param("add_logo",record.add_logo or '')
            config_parameters.set_param("clean_origin_folder", record.clean_origin_folder or '')
            config_parameters.set_param("automatic_processing", record.automatic_processing or '')
            config_parameters.set_param("send_to_gdrive", record.send_to_gdrive or '')
            config_parameters.set_param("gdrive_path", record.gdrive_path or '')
            config_parameters.set_param("feeder_file_path", record.feeder_file_path or '')
