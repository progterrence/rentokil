# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
import codecs
import re
import logging
from validate_email import validate_email
from email.message import Message
from odoo import models, fields, api, tools, _

_logger = logging.getLogger(__name__)

class MailMailDelivery(models.Model):
    _name = 'mail.mail.delivery'
    _description = 'Mail Mail delivery Status'

    name = fields.Char()
    mail_id = fields.Many2one('mail.mail')

    @api.model
    def create(self, vals):
        ret = super(MailMailDelivery, self).create(vals)
        print(vals)

        # email_address_not_found
        return ret


class MailThread(models.AbstractModel):
    _inherit = 'mail.thread'

    @api.model
    def message_route(self, message, message_dict, model=None, thread_id=None,
                      custom_values=None):
        if message.get('subject') == 'Delivery Status Notification (Failure)':
            if message.get('X-Failed-Recipients'):
                mail_message_id = self.env['mail.message'].create(
                    {'name': message.get('subject')})
                vals = {
                    'subject':message.get('subject'),
                    'from': message.get('from'),
                    'to': message.get('to'),
                    'body': message.get('ARC-Authentication-Results'),
                    'email_address_not_found': True,
                    'failed_emails': message.get('X-Failed-Recipients'),
                    'email_to_str': message.get('X-Failed-Recipients'),
                    'model': 'res.partner',
                    'mail_message_id': mail_message_id.id,
                    'state': 'exception'
                }
                self.env['mail.mail'].create(vals)
        ret = super(MailThread, self).message_route(
            message=message,
            message_dict=message_dict,
            model=model,
            thread_id=thread_id,
            custom_values=custom_values
        )
        return ret


class MailMail(models.Model):
    _inherit = 'mail.mail'
    _order = 'date desc'

    email_to_str = fields.Char(compute='_compute_email_to_str', string='Email To')
    recipient_ids_str = fields.Char(compute='_compute_email_to_str', string='Recipients')
    email_address_not_found = fields.Boolean()
    resolved = fields.Boolean()
    failed_emails = fields.Char()

    @api.multi
    def _compute_email_to_str(self):
        for this in self:
            this.email_to_str = "%s" % this.recipient_ids.mapped('email')
            this.recipient_ids_str = "%s" % this.recipient_ids.mapped('name')

    @api.model
    def create(self, vals):
        ret = super(MailMail, self).create(vals)
        ret.auto_delete = False
        return ret
