# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import os
import logging
_logger = logging.getLogger(__name__)
from odoo import models, fields, api, _


class FeederSyncWizard(models.TransientModel):
    _name = 'feeder.sync.wizard'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Feeder Sync Wizard'

    @api.model
    def _default_feeder_file_path(self):
        return self.env["ir.config_parameter"].sudo().get_param('feeder_file_path')

    name = fields.Char()
    feeder_file_path = fields.Char(
        default=_default_feeder_file_path
    )

    @api.multi
    def new_sync(self):
        Partner = self.env['res.partner']
        if self.feeder_file_path:
            for filename in os.listdir(self.feeder_file_path):
                file_path = os.path.join(self.feeder_file_path, filename)
                with open(file_path,'r') as f:  # open in readonly mode
                    for line in f.readlines():
                        try:
                            split_lines = line.split("|,|")
                            account_number = split_lines[0][1:].strip()
                            name = split_lines[3].strip()
                            email = split_lines[17].strip()
                            if not email:
                                email = "%s-MISSING-EMAIL@gmail.com" % account_number
                            if account_number and name and email:
                                existing_contact = Partner.search([('account_number', '=', account_number)])
                                _logger.info(
                                    "PARSED PARAMS: %s, %s, %s" % (account_number,name, email))
                                if not existing_contact:
                                    Partner.create({
                                        'name': name,
                                        'account_number': account_number,
                                        'email': email
                                    })
                                    _logger.error(
                                        "NEW PARTNER CREATED: %s, %s, %s" % (account_number, name, email))
                                else:
                                    _logger.info(
                                        "PARTNER EXISTS: %s, %s, %s" % (existing_contact.account_number, existing_contact.name, existing_contact.email))
                            else:
                                _logger.info(
                                    "FAILED TO CREATE PARTNER: ACCOUNT NUMBER: %s, NAME: %s, EMAIL:%s" % (
                                    account_number, name, email))
                        except Exception:
                            _logger.info(
                                    "FAILED TO CREATE PARTNER: %s " % Exception)

                #Delete the file
                try:
                    os.remove(file_path)
                    _logger.info("DELETING FILE: %s" % file_path)
                except OSError:
                    _logger.info("ERROR DELETING FILE: %s WITH ERROR %s" % (
                        file_path, OSError))

    @api.model
    def auto_process_feeder_files(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        _logger.info('==========================AUTOMATIC PROCESS FEEDER FILES ===============================')
        feeder_file_path = config_parameters.sudo().get_param('feeder_file_path')
        if not feeder_file_path:
            _logger.info('==========================NO FEEDER FILE PATH .===============================')
            return
        if not sorted(os.listdir(feeder_file_path), key=len):
            _logger.info('==========================NO FILES IN FEEDER FILE PATH .===============================')
            return
        new_feeder_processing = self.create({})
        new_feeder_processing.new_sync()
        _logger.info(
            '==========================FEEDER FILE SYNC COMPLETED .===============================')