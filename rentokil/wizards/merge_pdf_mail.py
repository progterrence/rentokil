# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import re
import os
import time
import codecs
import base64
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime
import shutil


class MergePdfMail(models.TransientModel):
    _name = 'merge.pdf.mail.wizard'
    _description = 'Merge PDF Mail Wizard'

    name = fields.Char()

    @api.multi
    def merge_pdf_files(self):
        # If merge_final_pdf is true, merge in destination
        mail_ids = self._context.get('active_ids')
        mails = self.env['mail.mail'].browse(mail_ids)
        attachments = mails.mapped('attachment_ids.datas')
        config_parameters = self.env["ir.config_parameter"].sudo()
        open_files = []
        merger = PdfFileMerger()
        import io
        n = 0
        for pdf_datas in attachments:
            n += 1
            file_path = 'C:/ACS/rentokil_acs_invoicing_file_%s.pdf' % n
            bytes = base64.b64decode(pdf_datas, validate=True)
            f = open(file_path, 'wb')
            f.write(bytes)
            f.close()
            open_file = open(file_path, 'rb')
            merger.append(open_file)
            try:
                os.remove(file_path)
                print('removed......')
            except OSError:
                pass
        final_file = "%s/%s" % (
            config_parameters.sudo().get_param('destination_folder'),
            'failed_files.pdf'
        )
        try:
            os.remove(final_file)
        except OSError:
            pass
        with open(final_file, "wb") as fout:
            merger.write(fout)
            fout.close()
        merger.close()
        for file in open_files:
            file.close()
        return True
        # return {'type': 'ir.actions.client', 'tag': 'reload'}
