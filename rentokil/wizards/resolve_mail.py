# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import re
import os
import time
import codecs
import base64
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime
import shutil


class ResolveMailMail(models.TransientModel):
    _name = 'resolve.mail.wizard'
    _description = 'Resolve Mail Wizard'

    name = fields.Char()

    @api.multi
    def resolve_mail(self):
        # If merge_final_pdf is true, merge in destination
        active_ids = self._context.get('active_ids')
        active_model = self._context.get('active_model')
        records = self.env[active_model].browse(active_ids)
        if records:
            if active_model == 'mail.mail':
                records.write({'resolved': True})
            else:
                records.write({'state': 'fixed'})