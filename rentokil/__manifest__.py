# Author: Damien Crier
# Author: Julien Coux
# Copyright 2016 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Rentokil1',
    'version': '13.0.1.1.0',
    'category': 'Reporting',
    'summary': 'Rentokil Financial Reports',
    'author': 'Terrence Nzaywa',
    "website": "https://sunflowerweb.nl/",
    'depends': [
        'contacts',
        'mail',
        'base',
        'muk_mail_branding',
        'muk_web_theme',
        'report_xlsx'
        # 'mail_tracking',
        # 'muk_autovacuum',
        # 'muk_branding',
        # 'muk_web_branding',
        # 'muk_web_theme_mail',
        # 'muk_web_utils'
    ],
    'external_dependencies': {
        'python': [
            'validate_email',
            # 'py3dns' # pip install py3dns==3.1.1a0
        ]
    },
    'data': [
        'data/ir_cron_automatic_invoice.xml',
        'data/res_users.xml',
        'security/ir.model.access.csv',
        'views/invoice_batch.xml',
        'views/res_config_settings.xml',
        'views/res_partner.xml',
        'views/invoice_batch_log.xml',
        'reports/export_invoice_error_xlsx.xml',
        'wizards/invoice_batch_wizard.xml',
        'wizards/merge_pdf_mail.xml',
        'wizards/resolve_mail.xml',
        'views/mail_mail.xml',
        'wizards/feeder_sync_wizard.xml'
    ],
    'installable': True,
    'license': 'AGPL-3',
}
