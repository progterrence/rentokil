# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import re
import os
import time
import codecs
import base64
from validate_email import validate_email
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import shutil

STATES = [
    ('draft', 'Draft'),
    ('inprogress', 'In Progress'),
    ('done', 'Done')
]

LOG_STATE = [
    ('fixed', 'Fixed'),
    ('not_fixed', 'Not Fixed')
]


class InvoiceBatch(models.Model):
    _name = 'invoice.batch'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Invoice Batch'
    _order = 'create_date desc'

    def _default_origin_folder(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('origin_folder')

    def _default_invoice_batch_folder(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('invoice_batch_folder')

    def _default_logo_file(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('logo_file')

    def _default_destination_folder(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('destination_folder')

    def _default_gdrive_path(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('gdrive_path')

    def _default_send_mail(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('send_mail')

    def _default_merge_pdf(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('merge_final_pdf')

    def _default_clean_origin_folder(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('clean_origin_folder')

    def _default_send_to_gdrive(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        return config_parameters.sudo().get_param('send_to_gdrive')

    name = fields.Char(required=True)
    origin_folder = fields.Char('Origin Folder', required=True, default=_default_origin_folder,
        placeholder='/home/user/Desktop/invoices')
    process_file = fields.Boolean('Process single file')
    invoice_batch_file = fields.Char('Invoice Batch File',
        placeholder='/home/user/Desktop/invoices/batch1.pdf')
    invoice_batch_folder = fields.Char('Invoice Batch Folder', default=_default_invoice_batch_folder,
        placeholder='/home/user/Desktop/invoices/')
    logo_file = fields.Char('Logo File', required=True, default=_default_logo_file,
        placeholder='/home/user/Desktop/invoices/logo1.pdf')
    destination_folder = fields.Char('Destination Folder', required=True, default=_default_destination_folder,
        placeholder='/home/user/Desktop/invoices')
    send_mail = fields.Boolean(default=_default_send_mail)
    send_to_gdrive = fields.Boolean(default=_default_send_to_gdrive)
    gdrive_path = fields.Char('GDrive Path', required=True, default=_default_gdrive_path,
        placeholder='/home/user/Desktop/invoices/gdrive')
    # auto_clean_folders = fields.Boolean(default=True)
    state = fields.Selection(STATES, default='draft')
    log_ids = fields.One2many('invoice.batch.log', 'batch_id')
    log_count = fields.Integer(compute='_compute_log_count')
    error_pages = fields.Char(compute='compute_error_pages')
    process_pages = fields.Char()
    process_attachment = fields.Boolean()
    merge_final_pdf = fields.Boolean(default=_default_merge_pdf)
    clean_origin_folder = fields.Boolean(default=_default_clean_origin_folder)
    processed_files = fields.Integer(default=0)

    @api.multi
    def _compute_log_count(self):
        for this in self:
            this.log_count = len(this.log_ids)

    @api.model
    def auto_create_new_batch(self):
        config_parameters = self.env["ir.config_parameter"].sudo()
        print('==========================AUTOMATIC INVOICE RUNNING===============================')
        invoice_batch_path = config_parameters.sudo().get_param('invoice_batch_folder')
        invoice_batch_path_unsigned = config_parameters.sudo().get_param('origin_folder_unsigned')
        if invoice_batch_path and invoice_batch_path_unsigned:
            pdf_files = [
                x for x in sorted(os.listdir(invoice_batch_path), key=len)
                if x.endswith(".pdf")
            ]
            pdf_unsigned_files = [
                x for x in sorted(os.listdir(invoice_batch_path_unsigned), key=len)
                if x.endswith(".pdf")
            ]
        else:
            print('==========================NO invoice_batch_folder OR origin_folder_unsigned.===============================')
            return
        if not invoice_batch_path:
            print('==========================NO INVOICE BATCH NO.===============================')
            return
        if not len(pdf_files):
            print('==========================NO FILES IN BATCH PATH .===============================')
            return
        if len(pdf_unsigned_files):
            print('==========================ESD IS STILL BUSY.===============================')
            return
        if config_parameters.sudo().get_param('automatic_processing'):
            inprocess = self.search([('state', '=', 'inprogress')])
            if not inprocess:
                print('==========================CREATING NEW BATCH .===============================')
                self.create_new_batch()
            else:
                print('==========================ANOTHER BATCH IN PROGRESS ===============================')

    @api.model
    def create_new_batch(self):
        data = {
            'name': 'New'
        }
        new = self.create(data)
        new.process_invoices()

    @api.multi
    def log_message(self, message, page=False, account_number=False,
                    partner=False, file_path=False):
        self.ensure_one()
        vals = dict(
            name=message,
            page=page,
            account_number=account_number,
            partner_id=partner.id if partner else False,
            batch_id=self.id,
            file_path=file_path
        )
        self.env['invoice.batch.log'].create(vals)

    @api.multi
    def get_gdrive_folder(self):
        self.ensure_one()
        if not os.path.exists(self.gdrive_path):
            raise UserError("Google drive cannot be located.")
        this_month = datetime.now().strftime('%B')
        this_year = datetime.now().strftime('%Y')
        folder = self.gdrive_path + '\\' + this_year + '\\' + this_month
        # folder = self.gdrive_path + '/' + this_year + '/' + this_month
        if not os.path.exists(folder):
            os.makedirs(folder)
        return folder

    @api.multi
    def get_file_name(self, file):
        return "%s" % file['filename']

    @api.multi
    def get_singlefiles(self):
        invoice_numbers = []
        singlefiles = []
        files = sorted(os.listdir(self.invoice_batch_folder), key=len)
        pdf_files = [x for x in files if x.endswith(".pdf")]
        for file in pdf_files:
            # path = "%s\%s" % (self.invoice_batch_folder, file) (WINDOWS)
            path = "%s/%s" % (self.invoice_batch_folder, file)
            final_file_path = "%s/%s" % (self.destination_folder, file)
            print(path)
            singleinvoice = self.read_pdf(path)
            if not singleinvoice:
                continue
            pages = range(singleinvoice.numPages)
            for i in pages:
                i = int(i)
                string = singleinvoice.getPage(i).extractText()
                invoice_number = False
                credit_number = False
                try:
                    invoice_number = re.search('Invoice Number(.*)Account', string).group(1)[:-8]
                except Exception as e:
                    print(e)
                if not invoice_number:
                    try:
                        invoice_number = re.search('Credit Number(.*)Account', string).group(1)[:-8]
                    except Exception as e:
                        print(e)

                try:
                    acc_no = re.search('Account No (.*)/', string).group(1).split()[0][:-6]
                    customer = self.env['res.partner'].search(
                        [('account_number', '=ilike', acc_no)], limit=1)
                except Exception as e:
                    print(e)
                #    raise UserError(e)
                # if no customer, ass user to create user with this account number:
                if not customer:
                    msg = "Customer does not exist [Invoice No: %s] consider adding one!" % invoice_number
                    self.log_message(msg, page=i, account_number=acc_no, file_path=final_file_path)
                if customer and not customer.email:
                    msg = "Customer email does not exist [Invoice No: %s] consider adding one!" % invoice_number
                    self.log_message(msg, page=i, account_number=acc_no, partner=customer, file_path=final_file_path)
                if invoice_number not in invoice_numbers:
                    invoice_numbers.append(invoice_number)
                    singlefiles.append(dict(
                        page=i,
                        filename=file,
                        customer=customer,
                        invoicenumber=invoice_number,
                        account_number=acc_no,
                        pagecontent=[singleinvoice.getPage(i)]
                    ))
                else:
                    existing_invoice = next(item for item in singlefiles if item["invoicenumber"] == invoice_number)
                    existing_invoice['pagecontent'].append(singleinvoice.getPage(i))
        return singlefiles

    @api.multi
    def split_files_add_logo(self, singlefiles):
        self.ensure_one()
        for file in singlefiles:
            print('===================================FILE=============%s' % file)
            self.processed_files += 1
            output = PdfFileWriter()
            for page in file['pagecontent']:
                output.addPage(page)
            singleinvoice = "%s/%s" % (self.origin_folder, self.get_file_name(file))
            with open(singleinvoice, "wb") as rowinvoice:
                output.write(rowinvoice)
                rowinvoice.close()
            try:
                singleInvoicepdfsiged = PdfFileReader(singleinvoice)
            except Exception as e:
                print(e)
                msg = "File cannot be read %s" % singleinvoice
                self.log_message(msg)

            with open(self.logo_file, "rb") as logo:
                logopdf = PdfFileReader(logo)
                try:
                    logopdf = PdfFileReader(logo)
                except Exception as e:
                    print(e)
                    msg = "File cannot be read %s" % logo
                    self.log_message(msg)
                # create a pdf writer object for the output file
                pdf_writer = PdfFileWriter()
                
                print('=========Adding Logo===========')
                for i in range(singleInvoicepdfsiged.numPages):
                    # get first page of the original PDF
                    page = singleInvoicepdfsiged.getPage(i)
                    # get first page of the logo PDF
                    first_page_logo = logopdf.getPage(0)
                    # merge the two pages
                    config_parameters = self.env["ir.config_parameter"].sudo()
                    if config_parameters.sudo().get_param('add_logo'):
                        page.mergePage(first_page_logo)
                    # add page
                    pdf_writer.addPage(page)

                singleinvoicename = "%s/%s" % (self.destination_folder, self.get_file_name(file))
                # if res not in invoice_numbers:
                with open(singleinvoicename, "wb") as output:
                    # write the watermarked file to the new file
                    pdf_writer.write(output)
                    output.close()
                logo.close()

    @api.multi
    def _get_email_body(self, customer):
        self.ensure_one()
        # a_month_before = datetime.now() - timedelta(days=30)
        a_month_before = datetime.now()
        this_month = a_month_before.strftime('%B')
        this_year = datetime.now().strftime('%Y')
        msg = """
Dear %s,
<br/><br/>
Thank you  for your continued support, find attached your invoice for %s, %s.
<br/><br/>
Kindly settle this invoice as per credit terms.
<br/><br/>
Bank account details,
<br/><br/>
Kenya Shillings (KES) Account<br/>
Account Name: RENTOKIL INITIAL KENYA LIMITED<br/>
Account Number: 0106093843300<br/>
SWIFT Code: SCBLKENXXXX<br/>
Bank Name: STANDARD CHARTERED BANK KENYA LIMITED<br/>
Branch Name: Industrial Area<br/>
Bank Address: P.O. Box 30003-00100 G.P.O NAIROBI-KENYA<br/>
<br/>

SAFARICOM MPESA PAYBILL,
<br/><br/>
PAY BILL NO: 552400
<br/><br/>

Please note this is an automatically generated mail, DO NOT reply to this address.
<br/><br/>
If you have any queries regarding your invoice, kindly email our Credit Control Department
on creditcontrol-ke@rentokil-initial.com or call us on 07030551000
<br/><br/>
Regards,<br/>
Accounts Receivables
# Rentokil-Initial Kenya Ltd
        """ % (customer.name.upper(), this_month, this_year)
        return msg


    @api.multi
    def send_mails(self, singlefiles):
        self.ensure_one()
        a_month_before = datetime.now() #- timedelta(days=30)
        this_month = a_month_before.strftime('%B')
        this_year = datetime.now().strftime('%Y')
        if self.send_mail:
            # send mail
            # subtype = 'mail.mt_comment' if self.send_mail else False
            # subtype = False
            for file in singlefiles:
                singleinvoicename = "%s/%s" % (self.destination_folder, self.get_file_name(file))
                if file['customer'] and file['customer'].email:
                    with open(singleinvoicename, "rb") as pdf_file1:
                        pdf_file = codecs.encode(pdf_file1.read(), 'base64')
                        customer = file['customer']
                        name = 'Invoice-%s.pdf' % file['invoicenumber']
                        attach1 = self.env['ir.attachment'].create({
                            'name': name,
                            'datas_fname': name,
                            'datas': pdf_file,
                            'res_model': 'res.partner', 'res_id': customer.id
                        })
                        vals = {
                            'model': 'res.partner',
                            'partner_ids': [(6, 0, [customer.id])],
                            'res_id': customer.id,
                            'subject': 'Rentokil-Initial Invoice - %s, %s' % (this_month, this_year),
                            'attachment_ids': [(6, 0, [attach1.id])],
                            'body': self._get_email_body(customer)
                        }
                        ctx = self.env.context.copy()
                        emails = customer.email.split(',')                        
                        mail = self.env['mail.compose.message'].create(vals)
                        mail.send_mail()
                        pdf_file1.close()

                print("sending mail: ", file['customer'].name, file['customer'].email)

    @api.multi
    def read_pdf(self, path):
        self.ensure_one()
        batchinvoice = False
        try:
            batchinvoice = PdfFileReader(path)
        except Exception as e:
            msg = "File cannot be read %s" % path
            self.log_message(msg)
            # raise UserError("No such file or directory %s" % path)
        return batchinvoice

    @api.multi
    def _merge_files(self, singlefiles):
        # If merge_final_pdf is true, merge in destination
        # x = [a for a in sorted(os.listdir(self.destination_folder), key=len) if a.endswith(".pdf")]
        open_files = []
        print('merge files .....')
        x = self.get_final_files(singlefiles)
        merger = PdfFileMerger()
        for pdf in x:
            open_file = open(pdf, 'rb')
            merger.append(open_file)
            open_files.append(open_file)
        file_name = 'final_merged_file_%s.pdf' % self.get_main_file_name(singlefiles)
        final_file = "%s\%s" % (self.get_gdrive_folder(), file_name)
        if not os.path.exists(final_file):
            with open(final_file, "wb") as fout:
                merger.write(fout)
                fout.close()
        merger.close()
        for file in open_files:
            file.close()

    @api.multi
    def get_final_files(self, singlefiles, add_origin=False):
        files = []
        for x in singlefiles:
            path = "%s\%s" % (self.destination_folder, x['filename'])
            files.append(path)
            if add_origin:
                path1 = "%s\%s" % (self.origin_folder, x['filename'])
                files.append(path1)
        return files

    @api.multi
    def get_original_files(self, singlefiles):
        files = []
        filenames = sorted(os.listdir(self.invoice_batch_folder), key=len)
        for x in filenames:
            path = "%s\%s" % (self.invoice_batch_folder, x)
            files.append(path)
        config_parameters = self.env["ir.config_parameter"].sudo()
        origin_folder_in = config_parameters.sudo().get_param('origin_folder_in')
        if origin_folder_in:
            in_filenames = sorted(os.listdir(origin_folder_in), key=len)
            for x in in_filenames:
                path = "%s\%s" % (origin_folder_in, x)
                files.append(path)
        return files

    @api.multi
    def get_main_file_name(self, singlefiles):
        # filename = "%s.pdf" % singlefiles[0]['filename'][:-9]
        filename = "%s" % singlefiles[0]['filename']
        return filename

    @api.multi
    def delete_split_files(self, singlefiles, origin=False, final=False):
        x = []
        # if final:
        #     x = self.get_final_files(singlefiles, add_origin=True)
        if origin:
            x = self.get_original_files(singlefiles)
        for finished_pdf in x:
            if os.path.exists(finished_pdf):
                os.remove(finished_pdf)

    @api.multi
    def process_invoices(self):
        self.ensure_one()
        start = time.time()
        self.state = 'inprogress'
        # self.split_single_files()
        if not self.invoice_batch_folder:
            raise UserError("Set the invoice batch folder path")
        singlefiles = self.get_singlefiles()
        if not singlefiles:
            return
        print(singlefiles)
        self.name = self.get_main_file_name(singlefiles)

        self.split_files_add_logo(singlefiles)

        # send mail
        self.send_mails(singlefiles)

        # copy all files in destination folder to gdrive
        if self.send_to_gdrive and self.gdrive_path:
            gdrive_path = self.get_gdrive_folder
            # if gdrive_path:
            #     # files = os.listdir(self.destination_folder)
            #     for file in singlefiles:
            #         file_name = self.get_file_name(file)
            #         final_name = "%s-%s" % (file['invoicenumber'], file['filename'])
            #         gpath = gdrive_path() + '\\' + final_name
            #         print(self.destination_folder + '\\' + file_name, gpath)
            #         shutil.copy(self.destination_folder + '\\' + file_name, gpath)
        if self.merge_final_pdf:
            self._merge_files(singlefiles)
            # self.delete_split_files(singlefiles, final=True)
        if self.clean_origin_folder:
            self.delete_split_files(singlefiles, origin=True)
        self.state = 'done'
        self.message_post(
            subtype='mail.mt_comment',
            message_type='comment',
            body='New invoice batch processing completed ...',
        )
        end = time.time()
        totaltime = end - start
        print("Total Time: %s" % totaltime)

    @api.multi
    def split_single_files(self):
        files = sorted(os.listdir(self.invoice_batch_folder))
        for file in files:
            path = "%s\%s" % (self.invoice_batch_folder, file)
            inputpdf = PdfFileReader(open(path, "rb"))
            msg = "File cannot be read %s" % inputpdf
            self.log_message(msg)
            for i in range(inputpdf.numPages):
                output = PdfFileWriter()
                output.addPage(inputpdf.getPage(i))
                with open("%s\document-page%s.pdf" % (self.invoice_batch_folder, i), "wb") as outputStream:
                    output.write(outputStream)

    @api.multi
    def clear_all_logs(self):
        self.ensure_one()
        self.log_ids.unlink()

    @api.multi
    def clear_fixed_logs(self):
        self.ensure_one()
        self.log_ids.filtered(lambda x: x.state == 'fixed').unlink()

    @api.multi
    def check_if_logs_fixed(self):
        self.ensure_one()
        for log in self.log_ids.filtered(lambda x: x.state == 'not_fixed'):
            partner = self.env['res.partner'].search([('account_number', '=ilike', log.account_number)])
            if partner and partner.email:
                log.state = 'fixed'

    @api.multi
    def print_failed_pdf(self):
        self.ensure_one()
        pages = [{'file_path': x.file_path, 'page': int(x.page)} for x in self.log_ids]
        output = PdfFileWriter()
        for page in pages:
            path = page['file_path']
            inputpdf = PdfFileReader(open(path, "rb"))
            msg = "File cannot be read %s" % inputpdf
            self.log_message(msg)
            output.addPage(inputpdf.getPage(page['page']))
        with open("%s\merged_pdf_from_errors.pdf" % self.destination_folder, "wb") as outputStream:
            output.write(outputStream)

    @api.multi
    def export_errors_excel(self):
        self.ensure_one()
        return self.env.ref(
            'rentokil.export_invoice_error_xlsx').report_action(self)

    @api.depends('log_ids')
    def compute_error_pages(self):
        for this in self:
            pages = this.log_ids.mapped('page')
            str_pages = [str(x) for x in pages]
            if pages:
                this.error_pages = ', '.join(str_pages)

    @api.model
    def create(self, vals):
        ret = super(InvoiceBatch, self).create(vals)
        ret.message_post(
            subtype='mail.mt_comment',
            message_type='comment',
            body='New invoice batch created ...',
        )
        return ret


class InvoiceBatchLog(models.Model):
    _name = 'invoice.batch.log'
    _description = 'Invoice Batch Log'

    name = fields.Char()
    batch_id = fields.Many2one('invoice.batch')
    partner_id = fields.Many2one('res.partner', 'Customer')
    account_number = fields.Char()
    page = fields.Char()
    state = fields.Selection(LOG_STATE, default='not_fixed')
    file_path = fields.Char('')

