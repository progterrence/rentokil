# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from validate_email import validate_email
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    doc_count = fields.Integer(compute='_compute_attached_docs_count',
        string="Number of documents attached")
    account_number = fields.Char(required=True)
    _sql_constraints = [
        ('account_number_unique', 'unique(account_number)', 'Account Number must be Unique !'),
    ]

    def check_account_number_exists(self, acc):
        exists = self.search([('account_number', '=ilike', acc)])
        if exists:
            raise UserError('Account Number: %s exists!!' % acc)

    def verify_email(self, email):
        config_parameters = self.env["ir.config_parameter"].sudo()
        validate = config_parameters.sudo().get_param('validate_email')
        if validate:
            emails = email.split(',')
            for email in emails:
                if not validate_email(email, verify=True):
                    raise UserError('Email invalid or does not exist11: %s' % email)

    @api.model
    def create(self, vals):
        if 'account_number' in vals and vals['account_number'].strip():
            self.check_account_number_exists(vals['account_number'])
        else:
            raise UserError('Account Number for: %s !!' % vals['name'])
        if 'email' in vals and vals['email'].strip():
            self.verify_email(vals['email'])
        else:
            raise UserError('Email Missing for Account: Number: %s !!' % vals['account_number'])
        ret = super(ResPartner, self).create(vals)
        return ret

    @api.multi
    def write(self, vals):
        if 'account_number' in vals:
            self.check_account_number_exists(vals['account_number'])
        if 'email' in vals:
            self.verify_email(vals['email'])
        ret = super(ResPartner, self).write(vals)
        return ret

    def _compute_attached_docs_count(self):
        Attachment = self.env['ir.attachment']
        for partner in self:
            partner.doc_count = Attachment.search_count([
                '&',
                ('res_model', '=', 'res.partner'),
                ('res_id', '=', partner.id),
            ])

    @api.multi
    def attachment_tree_view(self):
        self.ensure_one()
        domain = [
            '&',
            ('res_model', '=', 'res.partner'),
            ('res_id', 'in', self.ids)
        ]
        return {
            'name': _('Attachments'),
            'domain': domain,
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'kanban,tree,form',
            'view_type': 'form',
            'help': _('''<p class="o_view_nocontent_smiling_face">
                           Documents are attached to the customer.
                       </p>'''),
            'limit': 80,
            'context': "{'default_res_model': '%s','default_res_id': %d}" % (
            self._name, self.id)
        }
