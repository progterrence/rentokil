# Copyright 2019 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import re
import os
import time
import codecs
import base64
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime
import shutil


class InvoiceBatchWizard(models.TransientModel):
    _name = 'invoice.batch.wizard'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Invoice Batch Wizard'

    name = fields.Char()

    @api.multi
    def new_batch(self):
        self.env['invoice.batch'].create_new_batch()
        return {'type': 'ir.actions.client', 'tag': 'reload'}
        # action = self.env.ref('rentokil.invoice_batch_action')
        # return action
